var bodyParser = require('body-parser');
var braintree = require('braintree');
var parseUrlEnconded = bodyParser.urlencoded({
    extended: false
});
var gateway;
module.exports = {
    clinetSetup  : function(client) {
      return function(req,res,next) { 
       gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
          merchantId: client.merchantId,
          publicKey: client.publicKey,
          privateKey: client.privateKey
        });
        next();
      }
    },
    getClinetToken: function(callback) {
        gateway.clientToken.generate({}, function(err, res) {
          callback({clientToken: res.clientToken});
        });
    },
    getTransaction : function(request,response.,callback) {
      var transaction = request.body;
        gateway.transaction.sale({
            amount: transaction.amount,
            paymentMethodNonce: transaction.payment_method_nonce
        }, function(err, result) {
            if (err) return callback(err,null) ;
            if (result.success) {
              return callback(null,result) ;
            } else {
               return callback(err,null) ; 
            }
        });
        next();
    }
}
