'use strict';
var express = require('express'),
bodyParser = require('body-parser'),
app = express(),
tree = require('./index'),
parseUrlEnconded = bodyParser.urlencoded({
  extended: false
}),
clinetId = { 
            merchantId:   'n66sjnwsxwnwcpdc',
            publicKey:    'kkk5mfqmsgsdh72v',
            privateKey:   'b0b561a5d3e398b3ade1ed31b6093f46'
          };

app.use(express.static('public'));
app.use(tree.clinetSetup(clinetId));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/checkout',function (request,response) {
  tree.getClinetToken(function(data) {
        response.render('index', {
        clientToken: data.clientToken
        amount:request.amount
    });
  })
});
app.post('/process', parseUrlEnconded,tree.getTransaction,function(request, response) {
  tree.getTransaction(request,response,function(err, result) {
            if (err) throw err;
            if (result) {
                console.log(result);
                response.sendFile('success.html', {
                    root: './public'
                });
            } else {
                response.sendFile('error.html', {
                    root: './public'
                });
            }
        });
  });
app.listen(3000, function () {
  console.log('Listening on port 3000');
});

module.exports = app;